package ru.tsc.karbainova.tm;

import java.util.Scanner;

import static ru.tsc.karbainova.tm.constant.TerminalConst.*;
import static ru.tsc.karbainova.tm.constant.TerminalConst.CMD_EXIT;

public class Application {
    public static void main(String[] args) {
        displayWelcome();
        run(args);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Enter command:");
            final String command = scanner.nextLine();
            parseArg(command);
        }
    }

    public static void exit() {
        System.exit(0);
    }

    private static void parseArg(final String param) {
        if (CMD_HELP.equals(param)) displayHelp();
        if (CMD_VERSION.equals(param)) displayVersion();
        if (CMD_ABOUT.equals(param)) displayAbout();
        if (CMD_EXIT.equals(param)) exit();

    }

        private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void run(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        parseArg(param);
        System.exit(0);
    }

    private static void displayHelp() {
        System.out.println(CMD_VERSION + " - Display program version. ");
        System.out.println(CMD_ABOUT + " - Display developer info. ");
        System.out.println(CMD_HELP + " - Display list of terminal commands. ");
        System.out.println(CMD_EXIT + " - Exit program. ");
    }

    private static void displayVersion() {
        System.out.println("1.0.0");
    }

    private static void displayAbout() {
        System.out.println("Developer: Mariya Karbainova");
        System.out.println("E-mail: mariya@karbainova");
    }
}
